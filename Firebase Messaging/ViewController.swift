 //
//  ViewController.swift
//  Firebase Messaging
//
//  Created by Rich Kolasa on 8/21/17.
//  Copyright © 2017 Rich Kolasa. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let realm = try! Realm()
    let labelColor = UserDefaults.standard.colorForKey(key: "titleColor")
    
    var message: String?
    var notificationTitle = String()
    var notificationBody: String? = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.textColor = labelColor
        
        let notificationObjects = realm.objects(NotificationObject.self)
        
        for object in notificationObjects {
            notificationTitle = object.title
            notificationBody = object.body
        }
    
        titleLabel.text = notificationTitle
        bodyLabel.text = notificationBody
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(updateLabel), name: .alertMessageKey, object: nil)
                
        let token = Messaging.messaging().fcmToken
        print("Token: \(token ?? "not there")")
    }

    @objc func updateLabel() {
        
        let notificationObject = NotificationObject()
        
        try! realm.write {
            realm.add(notificationObject)
            notificationObject.title = appDelegate.title
            notificationObject.body = appDelegate.body
        }
        
        if appDelegate.response?.actionIdentifier == "NEGATIVE_OUTCOME" {
            titleLabel.textColor = .red
        } else {
            titleLabel.textColor = .green
        }
        
        titleLabel.text = notificationObject.title
        bodyLabel.text = notificationObject.body
        
        UserDefaults.standard.storeColor(color: titleLabel.textColor, forKey: "titleColor")

    }
    
}

extension Notification.Name {
    static let alertMessageKey = Notification.Name("alertMessageKey")
}

 extension UserDefaults {
    
    func colorForKey(key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = data(forKey: key) {
            color = NSKeyedUnarchiver.unarchiveObject(with: colorData) as? UIColor
        }
        return color
    }
    
    func storeColor(color: UIColor?, forKey key: String) {
        var colorData: NSData?
        if let color = color {
            colorData = NSKeyedArchiver.archivedData(withRootObject: color) as NSData
        }
        set(colorData, forKey: key)
    }
 }
