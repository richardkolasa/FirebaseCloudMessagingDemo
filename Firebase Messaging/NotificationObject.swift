//
//  NotificationObject.swift
//  Firebase Messaging
//
//  Created by Rich Kolasa on 8/22/17.
//  Copyright © 2017 Rich Kolasa. All rights reserved.
//

import Foundation
import RealmSwift

class NotificationObject: Object {
    @objc dynamic var title = ""
    @objc dynamic var body: String? = nil
}
