# Firebase Cloud Messaging (FCM) Prototype
## Proof of concept Firebase Cloud Messaging app, including:
* Firebase/Firebase Messaging to handle push notifications, including those sent via HTTP request
* Realm.io for local/offline data storage
* Rich push notifications, including UI updates in response to notification actions